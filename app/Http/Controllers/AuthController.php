<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcomePage(Request $request){
        return view('welcome',["depan" => $request["first"],"belakang" => $request["last"]]);
    }
}
