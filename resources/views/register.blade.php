<!DOCTYPE html>
<html lang="en">

<head>
    <title>Tugas Laravel - Web Development, Week 3 Day 2</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form method="post" action="/welcome">
        @csrf
        <label for="first">First name:</label><br><br>
        <input type="text" placeholder="Nama Depan" name="first" id="first" required><br><br>
        <label for="last">Last name:</label><br><br>
        <input type="text" placeholder="Nama Belakang" name="last" id="last" required><br><br>

        <label for="gender" >Gender:</label><br><br>
        <input type="radio" name="gender" value="male" required> Male<br>
        <input type="radio" name="gender" value="female" required> Female<br>
        <input type="radio" name="gender" value="other" required> Other<br><br>

        <label for="nationality">Nationality:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapura">Singapura</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Thailand">Thailand</option>
            <option value="Vietnam">Vietnam</option>
            <option value="Amerika">Amerika</option>
        </select><br><br>

        <label for="language">Language spoken:</label><br><br>
        <input type="checkbox" name="language" value="Indonesia">Indonesia<br>
        <input type="checkbox" name="language" value="English">English<br>
        <input type="checkbox" name="language" value="Other">Other<br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea placeholder="Isikan bio anda disini!" name="bio" id="bio" required rows=8 cols=30></textarea><br>

        <input type="submit" value="Sign Up">
    </form>
</body>

</html>